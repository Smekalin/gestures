//
//  ViewController.m
//  GesturesHomeWork
//
//  Created by Sergey on 01/09/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIGestureRecognizerDelegate>
@property(weak, nonatomic) UIImageView* imageMan;
@property(assign, nonatomic) CGFloat scale;
@property(assign, nonatomic) CGFloat rotate;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(100, 100, 100, 100)];
    UIImage* image1 = [UIImage imageNamed:@"mario1.png"];
    UIImage* image2 = [UIImage imageNamed:@"mario2.png"];
    NSArray* images = @[image1, image2];
    imageView.animationImages = images;
    imageView.animationDuration = 1;
    [self.view addSubview:imageView];
    [imageView startAnimating];
    self.imageMan = imageView;
    
    // GestureTap
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                     action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
    
    //GestureSwipeRight
    UISwipeGestureRecognizer* swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    swipeGestureRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeGestureRight];
    
    //GestureSwipeLeft
    UISwipeGestureRecognizer* swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
    swipeGestureLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGestureLeft];
    
    //GestureDoubleTap
    UITapGestureRecognizer* doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(handleDoubleTap:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];
    
    [tapGesture requireGestureRecognizerToFail:doubleTapGesture];
    
    //GesturePinch
    UIPinchGestureRecognizer* pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self.view addGestureRecognizer:pinchGesture];
    pinchGesture.delegate = self;
    
    //RotatePinch
    UIRotationGestureRecognizer* rotateGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
    rotateGesture.delegate = self;
    [self.view addGestureRecognizer:rotateGesture];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Gesture

- (void)handleTap:(UITapGestureRecognizer*) tapGesture {
    [UIView animateWithDuration:0.4
                          delay:0
                        options:(UIViewAnimationOptionBeginFromCurrentState)
                     animations:^{
                         self.imageMan.center = [tapGesture locationInView:self.view];
                     }
                     completion:^(BOOL finished) {
                     }];
    
}

- (void)handleSwipeRight:(UISwipeGestureRecognizer*) swipeGestureRight {
    
    CGAffineTransform currentTransform = self.imageMan.transform;
    CGAffineTransform transform = CGAffineTransformRotate(currentTransform, M_PI_2);
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.imageMan.transform = transform;
                     }
                     completion:^(BOOL finished) {
                     }];
}

- (void)handleSwipeLeft:(UISwipeGestureRecognizer*) swipeGestureLeft {
    
    CGAffineTransform currentTransform = self.imageMan.transform;
    CGAffineTransform transform = CGAffineTransformRotate(currentTransform, -M_PI_2);
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.imageMan.transform = transform;
                     }
                     completion:^(BOOL finished) {
                     }];
}

- (void)handleDoubleTap:(UITapGestureRecognizer*) doubleTapGesture {
    NSLog(@"stop animating");
    [self.imageMan.layer removeAllAnimations];
}

- (void)handlePinch:(UIPinchGestureRecognizer*) pinchGesture {
    
    if (pinchGesture.state == UIGestureRecognizerStateBegan) {
        self.scale = 1.f;
    }
    
    CGFloat delta = 1.f + pinchGesture.scale - self.scale;
    
    CGAffineTransform currentTransform = self.imageMan.transform;
    
    CGAffineTransform transform = CGAffineTransformScale(currentTransform, delta, delta);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.imageMan.transform = transform;
    }];
}

- (void)handleRotate:(UIRotationGestureRecognizer*) rotateGesture {
    
    NSLog(@"rotate");
    if (rotateGesture.state == UIGestureRecognizerStateBegan) {
        self.rotate = 0;
    }
    
    CGFloat rotation = rotateGesture.rotation - self.rotate;
    
    CGAffineTransform currentTransform = self.imageMan.transform;
    
    CGAffineTransform transform = CGAffineTransformRotate(currentTransform, rotation);
    
    [UIView animateWithDuration:0.5 animations:^{
        self.imageMan.transform = transform;
    }];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
@end
























